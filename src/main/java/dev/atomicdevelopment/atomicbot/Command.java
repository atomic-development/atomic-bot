package dev.atomicdevelopment.atomicbot;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

public interface Command {
    void handleCommand(GuildMessageReceivedEvent event);
}
