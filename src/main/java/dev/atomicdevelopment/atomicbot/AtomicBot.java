package dev.atomicdevelopment.atomicbot;

import dev.atomicdevelopment.atomicbot.database.Mysql;
import dev.atomicdevelopment.atomicbot.threads.ModerationTask;
import com.moandjiezana.toml.Toml;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.cache.CacheFlag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.login.LoginException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Timer;

public class AtomicBot {

    private static final Logger logger = LoggerFactory.getLogger(AtomicBot.class);
    public static Toml toml;
    public static Mysql db;

    private static void generateConfig() {
        try {
            InputStream defaultConfig = AtomicBot.class.getResourceAsStream("/config.toml");
            assert defaultConfig != null;
            Files.copy(defaultConfig, Path.of("config.toml"));
        } catch (IOException error) {
            error.printStackTrace();
        }
        logger.error("Config could not be found, a new one has been generated, exiting...");
        System.exit(1);
    }

    public static void main(String[] args) {

        try {
            toml = new Toml().read(new FileInputStream("config.toml"));
        } catch (IOException error) {
            generateConfig();
        }

        db = new Mysql();

        // get discord token

        String token = toml.getString("token");
        if (token == null || token.length() == 0) {
            logger.error("No token found within the config json, exiting...");
            System.exit(1);
        }

        // Created JDA object

        JDA jda = null;
        try {
            jda = JDABuilder.create(token, GatewayIntent.GUILD_MESSAGES, GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_BANS)
                    .disableCache(CacheFlag.ACTIVITY, CacheFlag.VOICE_STATE, CacheFlag.CLIENT_STATUS, CacheFlag.EMOTE)
                    .setActivity(Activity.playing("Atomic Bot | Version 1.0.0"))
                    .addEventListeners(new Listeners())
                    .build();
        } catch (LoginException error) {
            logger.error("Invalid discord token has been passed! Exiting...");
            System.exit(1);
        }

        // Create tasks

        ModerationTask moderationTask = new ModerationTask(jda);
        Timer timer = new Timer("Moderation Thread");
        long period = toml.getLong("moderation.period");
        timer.schedule(moderationTask, 0L, period * 1000L);
    }
}
