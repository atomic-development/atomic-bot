package dev.atomicdevelopment.atomicbot;

public class DatabaseObject {
    public String ip;
    public int port;
    public String user;
    public String password;
    public String databaseName;
}
