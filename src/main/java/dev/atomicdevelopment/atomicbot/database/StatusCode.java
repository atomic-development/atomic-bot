package dev.atomicdevelopment.atomicbot.database;

public enum StatusCode {
    SUCCESS(0),
    NO_GUILD(1),
    UNKNOWN_SQL_EXCEPTION(2),
    EXISTS(3),
    NOT_EXISTS(4);

    int code;

    StatusCode(int code) {
        this.code = code;
    }
}
