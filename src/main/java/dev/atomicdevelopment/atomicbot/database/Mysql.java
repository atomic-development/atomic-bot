package dev.atomicdevelopment.atomicbot.database;

import dev.atomicdevelopment.atomicbot.DatabaseObject;
import dev.atomicdevelopment.atomicbot.AtomicBot;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Mysql {

    public Connection connection = getConnection();
    private static final Logger logger = LoggerFactory.getLogger(Mysql.class);

    public Mysql() {
        createTables();
        logger.info("Database tables loaded successfully");
    }

    private Connection getConnection() {
        try {
            if (!AtomicBot.toml.containsTable("database")) {
                logger.error("Could not find database table in config file");
                System.exit(1);
            }
            Map<String, Object> dbconfig = AtomicBot.toml.getTable("database").toMap();

            for (Map.Entry<String, Object> entry : dbconfig.entrySet()) {
                if (entry.getValue() == null) {
                    logger.error(String.format("Failed to get %s from config file, config returned null", entry.getKey()));
                    System.exit(1);
                }
            }

            DatabaseObject dbobject = AtomicBot.toml.getTable("database").to(DatabaseObject.class);
            Connection connection = DriverManager.getConnection(String.format("jdbc:mysql://%s:%s/%s", dbobject.ip, dbobject.port, dbobject.databaseName),
                    dbobject.user, dbobject.password);
            logger.info("Successfully connected to mysql database");
            return connection;
        } catch (SQLException error) {
            parseSQLException(error, true);
            return null;
        }
    }

    private void createTables() {
        try {
            Statement statement = connection.createStatement();
            String createTable = "CREATE TABLE IF NOT EXISTS ";

            // SQL tables
            String guildTable = createTable + "Guilds(id VARCHAR(30) PRIMARY KEY, name VARCHAR(255))";
            String prefixTable = createTable + "Prefixes(guildID VARCHAR(30) NOT NULL, PRIMARY KEY (guildID), FOREIGN KEY (guildID) REFERENCES Guilds(id), prefix VARCHAR(255))";
            String banTable = createTable + "GuildBans(guildID VARCHAR(30) NOT NULL, PRIMARY KEY (guildID), FOREIGN KEY (guildID) REFERENCES Guilds(id), userID VARCHAR(255), banTime INT)";
            String welcomeTable = createTable + "WelcomeChannel(guildID VARCHAR(30) NOT NULL, PRIMARY KEY (guildID), FOREIGN KEY (guildID) REFERENCES Guilds(id), channelID VARCHAR(255))";

            // Adding to the statements
            statement.addBatch(guildTable);
            statement.addBatch(prefixTable);
            statement.addBatch(banTable);
            statement.addBatch(welcomeTable);

            statement.executeBatch();
        } catch (SQLException error) {
            error.printStackTrace();
            System.exit(1);
        }
    }

    private void parseSQLException(SQLException exception, boolean fatal) {
        int errorCode = exception.getErrorCode();
        switch (errorCode) {
            case 1045:
                logger.error("Invalid mysql credentials");
                break;
            case 0:
                logger.error("Failed to connect to mysql database, is the ip valid?");
                this.connection = getConnection();  // Attempt to reconnect upon error (for example database went offline)
                break;
            case 1049:
                logger.error("Could not find the database defined in the config file");
                break;
            default:
                exception.printStackTrace();
                break;
        }
        if (fatal) System.exit(1);
    }

    public void insertGuild(String guildID, String guildName) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Guilds (id, name) VALUES (?, ?)");
            preparedStatement.setString(1, guildID);
            preparedStatement.setString(2, guildName);
            preparedStatement.executeUpdate();
            logger.info("Inserted new guild into database");
        } catch (SQLException error) {
            parseSQLException(error, false);
        }
    }

    public void updateGuildName(String guildID, String guildName) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE Guilds SET name=? WHERE id=?");
            preparedStatement.setString(1, guildName);
            preparedStatement.setString(2, guildID);
            preparedStatement.executeUpdate();
        } catch (SQLException error) {
            parseSQLException(error, false);
        }
    }

    public StatusCode insertPrefix(String guildID, String prefix) {
        PreparedStatement statement;
        try {
            statement = this.connection.prepareStatement("INSERT INTO Prefixes (guildID, prefix) VALUES(?, ?)");
            statement.setString(1, guildID);
            statement.setString(2, prefix);
            statement.executeUpdate();
            statement.close();
            logger.info("Inserted new prefix into database");
            return StatusCode.SUCCESS;
        } catch (SQLException error) {
            if (error instanceof SQLIntegrityConstraintViolationException) {
                return StatusCode.NO_GUILD;
            }
            parseSQLException(error, false);
            return StatusCode.UNKNOWN_SQL_EXCEPTION;
        }
    }

    public HashMap<String, String> getPrefixes() {
        try {
            PreparedStatement statement = this.connection.prepareStatement("SELECT guildID, prefix FROM Prefixes");
            ResultSet resultSet = statement.executeQuery();
            HashMap<String, String> returnValue = new HashMap<>();
            while (resultSet.next()) {
                returnValue.put(resultSet.getString(1), resultSet.getString(2));
            }
            return returnValue;
        } catch (SQLException error) {
            parseSQLException(error, true);
            return null;
        }
    }

    public boolean updatePrefix(String guildID, String newPrefix) {
        try {
            PreparedStatement statement = this.connection.prepareStatement("UPDATE Prefixes SET Prefix=? WHERE GuildID=?");
            statement.setString(1, newPrefix);
            statement.setString(2, guildID);
            statement.executeUpdate();
            return true;
        } catch (SQLException error) {
            parseSQLException(error, false);
            return false;
        }
    }

    public void removePrefix(String guildID) {
        try {
            PreparedStatement statement = this.connection.prepareStatement("DELETE FROM Prefixes WHERE GuildID=?");
            statement.setString(1, guildID);
            statement.executeUpdate();
        } catch (SQLException error) {
            parseSQLException(error, false);
        }
    }

    public boolean insertBan(String guildID, String userID, long time) {  // returns true if successfully removed otherwise returns false
        long banTime = (System.currentTimeMillis() / 1000L) + time;  // works out future time to unban the member
        try {
            PreparedStatement statement = this.connection.prepareStatement("INSERT INTO GuildBans(guildID, userID, banTime) VALUES (?, ?, ?)");
            statement.setString(1, guildID);
            statement.setString(2, userID);
            statement.setLong(3, banTime);
            statement.executeUpdate();
            return true;
        } catch (SQLException error) {
            parseSQLException(error, false);
            return false;
        }
    }

    public void removeBan(String guildID, String userID) {  // returns true if successfully removed otherwise returns false
        try {
            PreparedStatement statement = this.connection.prepareStatement("DELETE FROM GuildBans WHERE guildID=? AND userID=?");
            statement.setString(1, guildID);
            statement.setString(2, userID);
            statement.executeUpdate();
        } catch (SQLException error) {
            parseSQLException(error, false);
        }
    }

    @Nullable
    public HashSet<BanObject> getBans() {
        try {
            PreparedStatement statement = this.connection.prepareStatement("SELECT guildID, userID, banTime FROM GuildBans");
            ResultSet resultSet = statement.executeQuery();
            HashSet<BanObject> bans = new HashSet<>();
            while (resultSet.next()) {
                String guild = resultSet.getString(1);
                String user = resultSet.getString(2);
                long banTime = resultSet.getLong(3);
                bans.add(new BanObject(guild, user, banTime));
            }
            return bans;
        } catch (SQLException error) {
            parseSQLException(error, false);
            return null;
        }
    }

    public StatusCode hasWelcomeChannel(String guildID) {
        try {
            PreparedStatement statement = this.connection.prepareStatement("SELECT EXISTS(SELECT channelID FROM WelcomeChannel WHERE guildID=?)");
            statement.setString(1, guildID);
            ResultSet result = statement.executeQuery();
            result.next();
            if (result.getBoolean(1)) {
                return StatusCode.EXISTS;
            } else {
                return StatusCode.NOT_EXISTS;
            }
        } catch (SQLException error) {
            parseSQLException(error, false);
            return StatusCode.UNKNOWN_SQL_EXCEPTION;
        }
    }

    private boolean insertWelcomeChannel(String guildID, String channelID) {  // returns false if fails
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO WelcomeChannel(guildID, channelID) VALUES (?, ?)");
            statement.setString(1, guildID);
            statement.setString(2, channelID);
            statement.executeUpdate();
            return true;
        } catch (SQLException error) {
            parseSQLException(error, false);
            return false;
        }
    }

    private boolean updateWelcomeChannel(String guildID, String channelID) {  // returns false if fails
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE WelcomeChannel SET channelID=? WHERE guildID=?");
            statement.setString(1, channelID);
            statement.setString(2, guildID);
            statement.executeUpdate();
            return true;
        } catch (SQLException error) {
            parseSQLException(error, false);
            return false;
        }
    }

    public boolean setWelcomeChannel(String guildID, String channelID) {  // returns false if fails
        StatusCode code = hasWelcomeChannel(guildID);

        boolean success = false;
        switch (code) {

            case UNKNOWN_SQL_EXCEPTION:
                break;

            case EXISTS:
                success = updateWelcomeChannel(guildID, channelID);
                break;

            case NOT_EXISTS:
                success = insertWelcomeChannel(guildID, channelID);
                break;
        }

        return success;
    }

    public HashMap<String, String> getWelcomeChannels() {
        HashMap<String, String> returnHashmap = new HashMap<>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT guildID, channelID FROM WelcomeChannel");
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                returnHashmap.put(result.getString(1), result.getString(2));
            }
            return returnHashmap;
        } catch (SQLException error) {
            parseSQLException(error, true);
            return null;
        }
    }
}
