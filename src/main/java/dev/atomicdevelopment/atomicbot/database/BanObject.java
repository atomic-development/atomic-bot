package dev.atomicdevelopment.atomicbot.database;

public class BanObject {
    public String guildID;
    public String userID;
    public long banTime;

    BanObject(String guildID, String userID, long banTime) {
        this.guildID = guildID;
        this.userID = userID;
        this.banTime = banTime;
    }
}
