package dev.atomicdevelopment.atomicbot;

public enum TimeValues {

    W("week", 604800),
    D("day", 86400),
    H("hour", 3600),
    M("minute", 60),
    S("second", 1);

    String name;
    long seconds;

    TimeValues(String name, long seconds) {
        this.name = name;
        this.seconds = seconds;
    }

    public long getSeconds(int time) {
        return time * this.seconds;
    }
}
