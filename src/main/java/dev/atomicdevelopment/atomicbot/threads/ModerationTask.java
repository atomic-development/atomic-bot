package dev.atomicdevelopment.atomicbot.threads;

import dev.atomicdevelopment.atomicbot.database.BanObject;
import dev.atomicdevelopment.atomicbot.AtomicBot;
import net.dv8tion.jda.api.JDA;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Objects;
import java.util.TimerTask;

public class ModerationTask extends TimerTask {

    private final JDA jda;
    private static final Logger logger = LoggerFactory.getLogger(ModerationTask.class);

    public ModerationTask(JDA jda) {
        this.jda = jda;
    }

    @Override
    public void run() {
        int unbanCount = 0;
        logger.info("Beginning searching for unbans");
        HashSet<BanObject> bans = AtomicBot.db.getBans();
        if (bans == null) {
            return;
        }
        for (BanObject ban : bans) {
            if (System.currentTimeMillis() / 1000L >= ban.banTime) {
                AtomicBot.db.removeBan(ban.guildID, ban.userID);
                Objects.requireNonNull(Objects.requireNonNull(jda.getGuildById(ban.guildID)).unban(ban.userID)).queue();
                unbanCount++;
            }
        }
        logger.info(String.format("Finished searching for unbans, unbanned %d users", unbanCount));
    }
}
