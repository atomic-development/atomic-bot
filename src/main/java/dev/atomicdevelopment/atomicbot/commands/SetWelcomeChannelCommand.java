package dev.atomicdevelopment.atomicbot.commands;

import dev.atomicdevelopment.atomicbot.Command;
import dev.atomicdevelopment.atomicbot.commands.utils.EmbedReplies;
import dev.atomicdevelopment.atomicbot.AtomicBot;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

public class SetWelcomeChannelCommand implements Command {

    @Override
    public void handleCommand(GuildMessageReceivedEvent event) {
        String message = event.getMessage().getContentRaw();
        TextChannel channel = event.getChannel();
        EmbedBuilder embed;
        EmbedReplies embedReplies = new EmbedReplies();

        int firstSpace = message.indexOf(" ");
        if (firstSpace == -1) {
            embed = embedReplies.errorEmbed("You have not passed a channel to set as the welcome channel", event.getAuthor());
            channel.sendMessage(embed.build()).queue();
            return;
        }
        firstSpace++;  // move the index one in front of the first space
        int secondSpace = message.indexOf(" ", firstSpace);
        String messageChannel;
        if (secondSpace == -1) {
            messageChannel = message.substring(firstSpace);
        } else {
            messageChannel = message.substring(firstSpace, secondSpace);
        }

        String channelID;
        int hashtagIndex = messageChannel.indexOf("#");
        if (hashtagIndex == -1) {
            channelID = messageChannel;
        } else {
            channelID = messageChannel.substring(hashtagIndex + 1, messageChannel.indexOf(">"));
        }

        TextChannel welcomeChannel = event.getGuild().getTextChannelById(channelID);
        if (welcomeChannel != null) {
            boolean successful = AtomicBot.db.setWelcomeChannel(event.getGuild().getId(), channelID);

            if (successful) {
                embed = embedReplies.successEmbed(String.format("Successfully set the welcome channel to **#%s**", welcomeChannel.getName()), event.getAuthor());
            } else {
                embed = embedReplies.errorEmbed("Failed to add welcome channel to database, please report this to the bot host", event.getAuthor());
            }
        } else {
            embed = embedReplies.errorEmbed("Could not find a channel with the id '%s' in your discord server", event.getAuthor());
        }
        channel.sendMessage(embed.build()).queue();
    }
}
