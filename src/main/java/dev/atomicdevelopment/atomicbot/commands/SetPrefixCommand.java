package dev.atomicdevelopment.atomicbot.commands;

import dev.atomicdevelopment.atomicbot.Command;
import dev.atomicdevelopment.atomicbot.commands.utils.EmbedReplies;
import dev.atomicdevelopment.atomicbot.AtomicBot;
import dev.atomicdevelopment.atomicbot.Listeners;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

public class SetPrefixCommand implements Command {

    @Override
    public void handleCommand(GuildMessageReceivedEvent event) {
        EmbedBuilder embed;
        EmbedReplies embedReplies = new EmbedReplies();
        TextChannel channel = event.getChannel();
        String message = event.getMessage().getContentRaw();
        int beginPrefix = message.indexOf(" ");
        int endPrefix = message.indexOf(" ", beginPrefix + 1);
        if (beginPrefix == -1) {
            embed = embedReplies.errorEmbed("You have not provided a new prefix for the bot", event.getAuthor());
            channel.sendMessage(embed.build()).queue();
            return;
        }
        String prefix;
        if (endPrefix == -1) {
            prefix = message.substring(beginPrefix + 1);
        } else {
            prefix = message.substring(beginPrefix + 1, endPrefix);
        }
        String guildID = event.getGuild().getId();
        boolean success = AtomicBot.db.updatePrefix(guildID, prefix);
        if (success) {
            embed = embedReplies.successEmbed(String.format("The prefix has been successfully changed to **%s**", prefix), event.getAuthor());
            boolean existingPrefix = Listeners.prefixes.containsKey(guildID);
            if (existingPrefix) {
                Listeners.prefixes.replace(guildID, prefix);
            } else {
                Listeners.prefixes.put(guildID, prefix);
            }
        } else {
            embed = embedReplies.errorEmbed("Prefix failed to be changed, please report this issue to a developer", event.getAuthor());
        }
        channel.sendMessage(embed.build()).queue();
    }
}
