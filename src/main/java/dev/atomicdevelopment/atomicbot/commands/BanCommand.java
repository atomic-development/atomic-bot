package dev.atomicdevelopment.atomicbot.commands;

import dev.atomicdevelopment.atomicbot.Command;
import dev.atomicdevelopment.atomicbot.commands.utils.DurationParser;
import dev.atomicdevelopment.atomicbot.commands.utils.EmbedReplies;
import dev.atomicdevelopment.atomicbot.AtomicBot;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.Arrays;

public class BanCommand implements Command {

    // <command invoke> <member> <duration (optional)> <reason (optional)>
    @Override
    public void handleCommand(GuildMessageReceivedEvent event) {
        Message message = event.getMessage();
        String[] args = message.getContentRaw().split(" ");
        TextChannel channel = event.getChannel();
        EmbedReplies embedReplies = new EmbedReplies();
        EmbedBuilder embed;

        if (args.length == 1) {  // checks to see if a member was passed
            embed = embedReplies.errorEmbed("You have not specified a member to ban, please @<member>#xxxx", event.getAuthor());
            channel.sendMessage(embed.build()).queue();
            return;
        }

        // Parse ban duration
        long banDuration;
        if (args.length < 3) {
            banDuration = 3600;
        } else {
            banDuration = DurationParser.parseDuration(args[2]);

            if (banDuration == -1) {
                embed = embedReplies.errorEmbed("You have entered an invalid duration", event.getAuthor());
                channel.sendMessage(embed.build()).queue();
                return;
            }
        }

        // Get member from the message
        Member banMember;
        try {
            banMember = message.getMentionedMembers().get(0);
        } catch (IndexOutOfBoundsException error) {
            embed = embedReplies.errorEmbed("The member you have mentioned could not be found, or is invalid", event.getAuthor());
            channel.sendMessage(embed.build()).queue();
            return;
        }

        // Attempt to insert ban into database
        boolean successful = AtomicBot.db.insertBan(event.getGuild().getId(), banMember.getId(), banDuration);

        if (!successful) {
            embed = embedReplies.errorEmbed("Failed to update ban database, ban voided", event.getAuthor());
            channel.sendMessage(embed.build()).queue();
            return;
        }

        // Bans the user from the guild depending on if a reason was passed or not
        String reason = null;
        if (args.length < 4) {
            banMember.ban(0).queue();
        } else {
            reason = String.join(" ", Arrays.copyOfRange(args, 3, args.length));
           banMember.ban(0, reason).queue();
        }

        // Success message
        embed = embedReplies.successEmbed(String.format("Successfully banned the user %s from the discord server for the reason '%s'", banMember.getAsMention(), reason), event.getAuthor());
        embed.setFooter("Punishment dealt by " + message.getAuthor().getAsTag());
        channel.sendMessage(embed.build()).queue();
    }
}
