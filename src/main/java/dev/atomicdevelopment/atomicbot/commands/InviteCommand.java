package dev.atomicdevelopment.atomicbot.commands;

import dev.atomicdevelopment.atomicbot.Command;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.awt.Color;

public class InviteCommand implements Command {

    private static final String invite = "https://discord.com/api/oauth2/authorize?client_id=835239017254617149&permissions=8&scope=bot";

    @Override
    public void handleCommand(GuildMessageReceivedEvent event) {
        EmbedBuilder embed = new EmbedBuilder();
        embed.setTitle("Bot Invite");
        embed.setDescription(String.format("You can invite the bot [here](%s).", invite));
        embed.setFooter("Invite requested by " + event.getAuthor().getAsTag());
        embed.setColor(Color.BLUE);
        event.getChannel().sendMessage(embed.build()).queue();
    }
}
