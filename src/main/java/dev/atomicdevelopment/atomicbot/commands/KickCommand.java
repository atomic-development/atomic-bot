package dev.atomicdevelopment.atomicbot.commands;

import dev.atomicdevelopment.atomicbot.Command;
import dev.atomicdevelopment.atomicbot.commands.utils.EmbedReplies;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.Arrays;
import java.util.List;

public class KickCommand implements Command {

    // <invoke> <member to kick> <reason (optional)>
    @Override
    public void handleCommand(GuildMessageReceivedEvent event) {
        Message message = event.getMessage();
        TextChannel channel = event.getChannel();
        String[] args = message.getContentRaw().split(" ");
        EmbedReplies embedReplies = new EmbedReplies();
        EmbedBuilder embed;
        List<Member> mentions = message.getMentionedMembers();

        if (args.length == 1 || mentions.size() == 0) {  // only contains the command invoke
            embed = embedReplies.errorEmbed("You have not specified a user, or the user you have specified is invalid", event.getAuthor());
            channel.sendMessage(embed.build()).queue();
            return;
        }
        Member targetMember = mentions.get(0);

        String reason = null;
        if (args.length < 3) {  // if no reason was passed
            targetMember.kick().queue();
        } else {  // if a reason was passed
            reason = String.join(" ", Arrays.copyOfRange(args, 2, args.length));
            targetMember.kick(reason).queue();
        }
        embed = embedReplies.successEmbed(String.format("Successfully kicked the user %s from the discord server for the reason '%s'", targetMember.getAsMention(), reason), event.getAuthor());
        channel.sendMessage(embed.build()).queue();
    }
}
