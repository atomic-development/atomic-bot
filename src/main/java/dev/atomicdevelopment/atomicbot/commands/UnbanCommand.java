package dev.atomicdevelopment.atomicbot.commands;

import dev.atomicdevelopment.atomicbot.Command;
import dev.atomicdevelopment.atomicbot.commands.utils.EmbedReplies;
import dev.atomicdevelopment.atomicbot.AtomicBot;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

public class UnbanCommand implements Command {

    // <invoke> <id> or <invoke> <mention(<@!id> or <@id>)>
    @Override
    public void handleCommand(GuildMessageReceivedEvent event) {
        TextChannel channel = event.getChannel();
        EmbedBuilder embed;
        Guild guild = event.getGuild();
        EmbedReplies embedReplies = new EmbedReplies();
        String message = event.getMessage().getContentRaw();
        int spaceIndex = message.indexOf(" ");

        // checks to see if a user has been passed
        if (spaceIndex == -1 || message.length() - 1 == spaceIndex) {
            embed = embedReplies.errorEmbed("You have not specified a user to unban", event.getAuthor());
            channel.sendMessage(embed.build()).queue();
            return;
        }

        int endIndex = message.indexOf(spaceIndex + 1);  // in case another space is passed, it will ignore it
        String user;
        String userID;
        if (endIndex == -1) {
            user = message.substring(spaceIndex + 1);
        } else {
            user = message.substring(spaceIndex + 1, endIndex);
        }

        if (!user.contains("<")) {  // if the user has been mentioned, it must contain a < therefore no other checks are needed
            userID = user;
        } else {
            userID = user.substring(3, user.length() - 1);
        }
        guild.unban(userID).queue();
        AtomicBot.db.removeBan(guild.getId(), userID);

        embed = embedReplies.successEmbed("Successfuly unbanned the user from the discord server", event.getAuthor());
        channel.sendMessage(embed.build()).queue();
    }
}
