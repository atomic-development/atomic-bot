package dev.atomicdevelopment.atomicbot.commands.utils;

import dev.atomicdevelopment.atomicbot.TimeValues;

public class DurationParser {

    public static long parseDuration(String duration) {  // Parses the duration, returns an int in seconds
        int durationLength = duration.length();
        String timeValue = duration.substring(durationLength - 1);
        int time = -1;

        try {  // converts the String to an int
            time = Integer.parseInt(duration.substring(0, durationLength - 1));
        } catch (NumberFormatException error) {
            return time;  // -1 is taken as an error
        }
        TimeValues timeValues = TimeValues.valueOf(timeValue.toUpperCase());

        return timeValues.getSeconds(time);
    }
}
