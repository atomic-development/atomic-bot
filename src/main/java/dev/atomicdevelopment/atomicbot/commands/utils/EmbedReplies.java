package dev.atomicdevelopment.atomicbot.commands.utils;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.User;
import org.jetbrains.annotations.Nullable;

import java.awt.*;
import java.time.Instant;

public class EmbedReplies {  // a bunch of predefined embeds

    private static final EmbedBuilder embed = new EmbedBuilder();

    public EmbedBuilder errorEmbed(String message, @Nullable User author) {
        embed.setTitle("An error has occurred");
        embed.setDescription(message);
        embed.setColor(Color.RED);
        embed.setTimestamp(Instant.now());
        if (author != null) {
            embed.setAuthor(author.getAsTag(), null, author.getAvatarUrl());
        }
        return embed;
    }

    public EmbedBuilder successEmbed(String message, @Nullable User author) {
        embed.setTitle("Success");
        embed.setDescription(message);
        embed.setColor(Color.GREEN);
        embed.setTimestamp(Instant.now());
        if (author != null) {
            embed.setAuthor(author.getAsTag(), null, author.getAvatarUrl());
        }
        return embed;
    }
}
