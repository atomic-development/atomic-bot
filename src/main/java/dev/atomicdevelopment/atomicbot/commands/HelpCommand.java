package dev.atomicdevelopment.atomicbot.commands;

import dev.atomicdevelopment.atomicbot.Command;
import dev.atomicdevelopment.atomicbot.CommandObject;
import dev.atomicdevelopment.atomicbot.HelpObject;
import dev.atomicdevelopment.atomicbot.commands.utils.EmbedReplies;
import dev.atomicdevelopment.atomicbot.Listeners;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.awt.*;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class HelpCommand implements Command {

    public static HashMap<String, HelpObject> commandInfo = new HashMap<>();

    // <invoke> <category (optional)> <command(optional)>
    @Override
    public void handleCommand(GuildMessageReceivedEvent event) {
        EmbedBuilder embed = new EmbedBuilder();
        embed.setColor(new Color(105, 187, 158));
        embed.setTimestamp(Instant.now());
        embed.setAuthor(event.getAuthor().getAsTag(), null, event.getAuthor().getAvatarUrl());
        TextChannel channel = event.getChannel();
        String[] args = event.getMessage().getContentRaw().split(" ");
        String guildID = event.getGuild().getId();
        String prefix = Listeners.prefixes.get(guildID);
        EmbedReplies embedReplies = new EmbedReplies();

        switch (args.length) {

            case 1:  // only contains the invoke
                embed.setTitle("Command categories:");
                for (Map.Entry<String, String> entry : Listeners.commandCategories.entrySet()) {
                    String name = entry.getKey();
                    String desc = entry.getValue();
                    embed.addField(name + ":", desc, false);
                    embed.setDescription(String.format("**For a list of commands within a category do `%shelp <category>`**", prefix));
                }
                break;

            case 2:  // contains a category
                String category = args[1];
                embed.setTitle("Commands for the category " + category);
                boolean found = false;
                for (Map.Entry<String, CommandObject> entry : Listeners.commands.entrySet()) {
                    if (entry.getValue().category.equalsIgnoreCase(category)) {
                        String name = entry.getKey();
                        boolean isAlias = false;
                        for (String alias : Listeners.aliases) {
                            if (name.equalsIgnoreCase(alias)) {
                                isAlias = true;
                                break;
                            }
                        }
                        if (isAlias) {
                            continue;
                        }
                        String helpMsg = commandInfo.get(name).helpText;
                        embed.addField(name, helpMsg, false);
                        found = true;
                    }
                    embed.setDescription(String.format("**For an explanation of a specific command do `%shelp %s <command>**",
                            prefix, category));
                }
                if (!found) {
                    embed = embedReplies.errorEmbed(String.format("Could not find the category **'%s'**", category), event.getAuthor());
                } else {
                    embed.setTitle("Commands for the category " + category);
                }
                break;

            case 3:  // contains a category and also a command name
                category = args[1];
                String command = args[2];
                embed.setTitle("Command info for the command " + command);

                String capitalisedCategory = category.substring(0, 1).toUpperCase() + category.substring(1);
                if (!Listeners.commandCategories.containsKey(capitalisedCategory)) {
                    embed = embedReplies.errorEmbed(String.format("The category **'%s'** does not exist", category), event.getAuthor());
                    channel.sendMessage(embed.build()).queue();
                    return;
                }

                HelpObject help = commandInfo.get(command);
                if (help == null) {
                    embed = embedReplies.errorEmbed(String.format("Could not find the command **'%s'** in the category **'%s'**", command, category), event.getAuthor());
                    channel.sendMessage(embed.build()).queue();
                    return;
                }

                embed.addField("Info", help.helpText, false);
                embed.addField("Usage", prefix + help.usage, false);
                StringBuilder aliases = new StringBuilder();

                if (help.aliases == null) {
                    aliases.append("No aliases");
                } else {
                    int lastIndex = help.aliases.length - 1;
                    int index = 0;
                    for (String alias : help.aliases) {
                        if (index == lastIndex) {
                            aliases.append(alias);
                        } else {
                            aliases.append(alias).append(", ");
                        }
                        index++;
                    }
                }
                embed.addField("Aliases", aliases.toString(), false);
                break;
        }
        channel.sendMessage(embed.build()).queue(); }
}
