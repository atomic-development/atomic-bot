package dev.atomicdevelopment.atomicbot.commands;

import dev.atomicdevelopment.atomicbot.Command;
import dev.atomicdevelopment.atomicbot.Listeners;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.awt.Color;
import java.time.Instant;

public class InfoCommand implements Command {

    private static final String OWNER = "AtomicProgramming";
    private static final String OWNER_URL = "https://cdn.discordapp.com/avatars/584859028329332758/395028412437b644c89f8385c665da59.png";
    // Repo is passed in as a hyperlink, discord accepts markdown
    private static final String REPO = "[Gitlab](https://gitlab.com/atomic-development/atomic-bot)";

    @Override
    public void handleCommand(GuildMessageReceivedEvent event) {
        TextChannel channel = event.getChannel();
        EmbedBuilder embed = new EmbedBuilder();
        embed.setTitle("Bot Info:");
        embed.addField("Bot Name:", "**AtomicBot**", false);
        embed.addField("Version:", "**1.0**", false);
        embed.addField("Current Prefix:", String.format("**%s**", Listeners.prefixes.get(event.getGuild().getId())), false);
        embed.addField("Guilds:", String.format("**Watching %d guild(s)**", event.getJDA().getGuilds().size()), false);
        embed.addField("Text Channels:", String.format("**Watching %d text channel(s)**", event.getJDA().getTextChannels().size()), false);
        embed.addField("Users", String.format("**Watching %d user(s)**", event.getJDA().getUsers().size()), false);
        embed.setTimestamp(Instant.now());
        embed.setAuthor("Owner: " + OWNER, null, OWNER_URL);
        embed.setColor(new Color(220, 103, 9));
        channel.sendMessage(embed.build()).queue();
    }
}
