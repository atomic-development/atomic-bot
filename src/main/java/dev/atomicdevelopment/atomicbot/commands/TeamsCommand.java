package dev.atomicdevelopment.atomicbot.commands;

import dev.atomicdevelopment.atomicbot.Command;
import dev.atomicdevelopment.teamsort.TeamSort;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.awt.*;
import java.util.*;
import java.util.List;

public class TeamsCommand implements Command {

    @Override
    public void handleCommand(GuildMessageReceivedEvent event) {
        EmbedBuilder embed = new EmbedBuilder();
        TextChannel channel = event.getChannel();
        int teamCount;
        String[] args = event.getMessage().getContentRaw().split(" ");  // command arguments, index 0 is the command invoke and should be ignored

        // Checks if the required arguments, the number of teams, was passed
        if (args.length < 2) {
            embed.setTitle("An error has occurred");
            embed.setColor(Color.RED);
            embed.setDescription("You have not passed the required arguments, please check the help command for more info");
            channel.sendMessage(embed.build()).queue();
            return;
        }

        // Attempt to convert the team count (which is passed as a string) to an integer
        try {
            teamCount = Integer.parseInt(args[1]);
        } catch (NumberFormatException error) {
            embed.setTitle("An error has occurred");
            embed.setColor(Color.RED);
            embed.setDescription(String.format("Failed to convert team count, '%s' is not an integer", args[1]));
            channel.sendMessage(embed.build()).queue();
            return;
        }

        // Check to see if the Team Count is too high, or low
        if (teamCount < 2 || teamCount > 10) {
            embed.setTitle("An error has occurred");
            embed.setColor(Color.RED);
            embed.setDescription("You may only have 2 -> 10 teams");
            channel.sendMessage(embed.build()).queue();
        }

        List<String> members;
        if (args.length > 3) {
            members = Arrays.asList(args).subList(2, args.length);
        } else {
            VoiceChannel vc = event.getMember().getVoiceState().getChannel();
            if (vc == null) {  // checks to see if the user is in a vc
                return;
            } else {
                List<Member> membersObject = new ArrayList<>(vc.getMembers());
                members = new ArrayList<>();
                for (Member member : membersObject) {
                    members.add(member.getAsMention());
                }
            }
        }

        TeamSort teamSort = new TeamSort(teamCount, members);
        int teamNum = 1;
        List<HashSet<String>> teams = teamSort.sort();
        for (HashSet<String> team : teams) {
            StringBuilder teamBuilder = new StringBuilder();
            for (String member : team) {
                teamBuilder.append(String.format("- %s\n", member));
            }
            embed.addField(String.format("Team %d:", teamNum), teamBuilder.toString(), true);
            teamNum++;
        }
        embed.setColor(Color.GREEN);
        channel.sendMessage(embed.build()).queue();
    }
}
