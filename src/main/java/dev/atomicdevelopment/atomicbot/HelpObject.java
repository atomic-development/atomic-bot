package dev.atomicdevelopment.atomicbot;

public class HelpObject {

    public String helpText;
    public String usage;
    public String[] aliases;

    public HelpObject(String helpText, String usage, String[] aliases) {
        this.helpText = helpText;
        this.usage = usage;
        this.aliases = aliases;
    }
}
