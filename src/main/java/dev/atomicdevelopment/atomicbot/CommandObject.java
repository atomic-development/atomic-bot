package dev.atomicdevelopment.atomicbot;

import net.dv8tion.jda.api.Permission;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandObject {

    public Command command;
    public String category;
    public List<Permission> requiredPermissions =  new ArrayList<>();

    public CommandObject(Command command, String category) {
        this.command = command;
        this.category = category;
    }

    public CommandObject addPermissions(Permission... permissions) {
        requiredPermissions.addAll(Arrays.asList(permissions));
        return this;
    }
}
