package dev.atomicdevelopment.atomicbot;

import dev.atomicdevelopment.atomicbot.commands.*;
import dev.atomicdevelopment.atomicbot.database.StatusCode;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.guild.update.GuildUpdateNameEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.awt.*;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import static dev.atomicdevelopment.atomicbot.commands.HelpCommand.commandInfo;

public class Listeners extends ListenerAdapter {

    // Hashmaps for caching data on memory for speed
    public static HashMap<String, String> commandCategories = new HashMap<>();
    public static HashMap<String, CommandObject> commands = new HashMap<>();
    public static HashMap<String, String> prefixes = AtomicBot.db.getPrefixes();
    public static HashSet<String> aliases = new HashSet<>();
    public static HashMap<String, String> welcomeChannels = AtomicBot.db.getWelcomeChannels();

    private static final Logger logger = LoggerFactory.getLogger(Listeners.class);
    public static final String DEFAULT_PREFIX = ".";

    @Override
    public void onReady(@NotNull ReadyEvent event) {

        long start = System.nanoTime();

        // add command categories to hashmap
        addCategory("Information", "A group of commands which provide information about the discord bot");
        addCategory("Moderation", "A group of command which aid in the moderation of your discord server");
        addCategory("Other", "A group of commands which do not have their own category");
        addCategory("Configuration", "A group of commands for configuring the bot for your discord server");

        // add commands to hashmap
        addCommand("invite", new CommandObject(new InviteCommand(), "Information"));
        addCommand("help", new CommandObject(new HelpCommand(), "Information"));
        addCommand("info", new CommandObject(new InfoCommand(), "Information"));

        // Commands which need permissions
        // TODO: add mute command with permission Permission.manage_messages
        addCommand("prefix", new CommandObject(new SetPrefixCommand(), "Other").addPermissions(Permission.ADMINISTRATOR));
        addCommand("ban", new CommandObject(new BanCommand(), "Moderation").addPermissions(Permission.BAN_MEMBERS));
        addCommand("kick", new CommandObject(new KickCommand(), "Moderation").addPermissions(Permission.KICK_MEMBERS));
        addCommand("unban", new CommandObject(new UnbanCommand(), "Moderation").addPermissions(Permission.BAN_MEMBERS));

        // Commands with several aliases are added several times
        CommandObject teamsCommand = new CommandObject(new TeamsCommand(), "Other");
        addCommand("teams", teamsCommand);
        addCommand("team", teamsCommand);
        addCommand("t", teamsCommand);
        addAliases("team", "t");  // adds aliases to stop them being added to category list see: HelpCommand.class

        // Commands with permissions and aliases
        CommandObject setWelcomeChannelCommand = new CommandObject(new SetWelcomeChannelCommand(), "Configuration").addPermissions(Permission.ADMINISTRATOR);
        addCommand("setwelcomechannel", setWelcomeChannelCommand);
        addCommand("swc", setWelcomeChannelCommand);
        addAliases("swc");

        // add command info to hashmap
        // Aliases are optional, and therefore any commands without aliases should pass null
        addHelp("info", new HelpObject("Information about AtomicBot", "info", null));
        addHelp("invite", new HelpObject("Get the invite link for the discord bot", "invite", null));
        addHelp("prefix", new HelpObject("Change the prefix of the discord bot", "prefix <new prefix>", null));
        addHelp("help", new HelpObject("This command", "help", null));
        addHelp("teams", new HelpObject("Sort players into teams", "teams <team count> <member> <member>...", new String[]{"team", "t"}));
        addHelp("ban", new HelpObject("Ban a user from the discord server", "ban <member> <duration (optional)> <reason (optional)>", null));
        addHelp("kick", new HelpObject("Kick a user from the discord server", "kick <member> <reason (optional)>", null));
        addHelp("unban", new HelpObject("Unban a user from the discord server", "unban <member#xxxx>", null));
        addHelp("setwelcomechannel", new HelpObject("Set the welcome channel for join messages", "setwelcomechannel <channel>", new String[]{"swc"}));

        long end = System.nanoTime();
        logger.info(String.format("Commands loaded in %sms", (int) ((end - start) / 1000 / 1000)));  // converts nano --< milli
        logger.info("Successfully logged into discord as " + event.getJDA().getSelfUser().getAsTag());
    }

    private void addCommand(String commandName, CommandObject commandObject) {
        commands.put(commandName, commandObject);
    }

    private void addHelp(String name, HelpObject help) {
        commandInfo.put(name, help);
    }

    private void addCategory(String name, String desc) {
        commandCategories.put(name, desc);
    }

    public void addAliases(String... names) {
        aliases.addAll(Arrays.asList(names));
    }

    @Nullable
    public CommandObject getCommand(String commandName) {
        return commands.getOrDefault(commandName, null);
    }

    @Override
    public void onGuildMessageReceived(@NotNull GuildMessageReceivedEvent event) {  // invoked when a new message is sent

        // The following code below handles the commands

        User user = event.getAuthor();
        if (event.isWebhookMessage() || user.isBot()) return;
        String guildID = event.getGuild().getId();
        String message = event.getMessage().getContentRaw();
        if (message.length() == 0) return;
        String prefix = prefixes.getOrDefault(guildID, null);
        if (prefix == null) {
            prefix = DEFAULT_PREFIX;
            prefixes.put(guildID, DEFAULT_PREFIX);
            StatusCode code = AtomicBot.db.insertPrefix(guildID, DEFAULT_PREFIX);
            if (code == StatusCode.NO_GUILD) {
                AtomicBot.db.insertGuild(guildID, event.getGuild().getName());
                AtomicBot.db.insertPrefix(guildID, DEFAULT_PREFIX);
            }
        }
        int prefixLength = prefix.length();
        if (!(message.substring(0, prefixLength).equals(prefix))) return;

        int commandEnd = message.indexOf(" ");
        CommandObject commandObject;
        if (commandEnd == -1) {
            commandObject = getCommand(message.substring(prefixLength));
        } else {
            commandObject = getCommand(message.substring(prefixLength, commandEnd));
        }

        EmbedBuilder embed = new EmbedBuilder();
        TextChannel channel = event.getChannel();
        if (commandObject == null) {
            embed.setTitle("Invalid command");
            embed.setDescription("The command you have entered could not be found");
            embed.setColor(Color.RED);
            channel.sendMessage(embed.build()).queue();
        } else {
            Member member = event.getMember();
            if (member == null) {
                return;
            }
            for (Permission permission : commandObject.requiredPermissions) {
                if (!member.hasPermission(permission)) {
                    embed.setTitle("Insufficient permission(s)");
                    embed.setDescription(String.format("You do not have the permission '%s', which is required to use this command", permission.toString().toLowerCase()));
                    embed.setColor(Color.RED);
                    channel.sendMessage(embed.build()).queue();
                    return;
                }
            }
            commandObject.command.handleCommand(event);
        }
    }

    @Override
    public void onGuildLeave(GuildLeaveEvent event) {  // invoked when the bot leaves a discord server

        String guildID = event.getGuild().getId();

        // Remove prefix - allows reset of prefix if lost

        prefixes.remove(guildID);  // removes prefix from hashmap
        AtomicBot.db.removePrefix(guildID);  // removes prefix from database

    }

    @Override
    public void onGuildUpdateName(@NotNull GuildUpdateNameEvent event) {  // invoked when a guild name changes

        // Update name stored in database
        AtomicBot.db.updateGuildName(event.getGuild().getId(), event.getNewName());
    }

    @Override
    public void onGuildMemberJoin(@NotNull GuildMemberJoinEvent event) {
        String welcomeChannelID = welcomeChannels.get(event.getGuild().getId());
        if (welcomeChannelID == null) {
            return;
        }
        TextChannel welcomeChannel = event.getGuild().getTextChannelById(welcomeChannelID);

        EmbedBuilder embed = new EmbedBuilder();
        embed.setTitle("A new user has joined");
        embed.setAuthor(event.getMember().getUser().getAsTag(), null, event.getMember().getUser().getAvatarUrl());
        embed.setColor(new Color(115, 27, 173));
        embed.setTimestamp(Instant.now());
        embed.setDescription(String.format("Welcome to %s, enjoy your stay!", event.getGuild().getName()));
        assert welcomeChannel != null;
        welcomeChannel.sendMessage(embed.build()).queue();
    }
}
