# Atomic Bot
## About:
AtomicBot is a discord bot which is the java continuation of team bot, porting some features but overall building on it as a whole, due to the move from python to java, AtomicBot is much faster, and furthermore can be distributed as an executable instead of text files

## Contributing:
You are free to contribute to the discord bot, feel free to add commands and then submit a merge request for review. Alterations to the command handler however could be problematic, so it is not advised.
